from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
import psycopg2
import random
import csv
import json
import uuid
import os

app = Flask(
    __name__,
    static_folder="../frontend/build/static",
    template_folder="../frontend/build",
)
CORS(app)

connection = psycopg2.connect(
    host=os.environ.get("HOST"),
    port=os.environ.get("PORT"),
    user="homefarmer",
    password=os.environ.get("PASSWORD"),
    database="postgres",
)
cursor = connection.cursor()


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


plant_cols = (
    "id",
    "common_name",
    "genus",
    "family",
    "scientific_name",
    "image_url",
    "wiki_url",
    "vegetable",
)
recipe_cols = (
    "id",
    "label",
    "yield",
    "ingredient_lines",
    "calories",
    "total_time",
    "source",
    "url",
    "image",
)
city_cols = (
    "id",
    "city_name",
    "state_name",
    "state_code",
    "latitude",
    "longitude",
    "min_temp",
    "max_temp",
    "humidity",
    "pressure",
    "image_url",
)


def valid_uuid(id):
    try:
        val = uuid.UUID(id, version=4)
    except ValueError:
        return False

    return str(val) == id


@app.route("/api/Plants")
def get_all_plants():
    # db.session.query(Plants).all()
    cursor.execute('SELECT * FROM public."Plants"')
    plants = cursor.fetchall()

    result = {"plants": []}

    for plant in plants:
        plant_obj = {}
        for i in range(len(plant_cols)):
            plant_obj[plant_cols[i]] = plant[i]
        result["plants"].append(plant_obj)

    return jsonify(result), 200


@app.route("/api/Plants/<string:id>")
def get_plant_by_id(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute("SELECT * FROM public.\"Plants\" WHERE id = '{}'".format(id))
    plant = cursor.fetchall()

    if len(plant) == 0:
        cursor.execute("SELECT * FROM public.\"Plants\" WHERE id = '{}'".format(id))
        plant = cursor.fetchall()
        if len(plant) == 0:
            return jsonify(error="Plant id not found"), 404

    result = {}
    for i in range(len(plant_cols)):
        result[plant_cols[i]] = plant[0][i]

    return jsonify(result), 200


@app.route("/api/Plants/Recipes/<string:id>")
def get_recipes_for_plant(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Recipes".* FROM (public."plants_recipes" INNER JOIN public."Recipes" ON public."plants_recipes".recipe_id=public."Recipes".id) WHERE public."plants_recipes".plant_id = \'{}\''.format(
            id
        )
    )
    recipes = cursor.fetchall()

    if len(recipes) == 0:
        return jsonify(error="Plant id not found"), 404

    result = {"recipes": []}

    for recipe in recipes:
        recipe_obj = {}
        for i in range(len(recipe_cols)):
            recipe_obj[recipe_cols[i]] = recipe[i]
        result["recipes"].append(recipe_obj)

    return jsonify(result), 200


@app.route("/api/Plants/Cities/<string:id>")
def get_cities_for_plant(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Cities".* FROM (public."plants_cities" INNER JOIN public."Cities" ON public."plants_cities".city_id=public."Cities".id) WHERE public."plants_cities".plant_id = \'{}\''.format(
            id
        )
    )
    cities = cursor.fetchall()

    if len(cities) == 0:
        return jsonify(error="Plant id not found"), 404

    result = {"cities": []}

    for city in cities:
        city_obj = {}
        for i in range(len(city_cols)):
            city_obj[city_cols[i]] = city[i]
        result["cities"].append(city_obj)

    return jsonify(result), 200


@app.route("/api/Recipes")
def get_all_recipes():
    # db.session.query(Plants).all()
    cursor.execute('SELECT * FROM public."Recipes"')
    recipes = cursor.fetchall()

    result = {"recipes": []}

    for recipe in recipes:
        recipe_obj = {}
        for i in range(len(recipe_cols)):
            recipe_obj[recipe_cols[i]] = recipe[i]
        result["recipes"].append(recipe_obj)

    return jsonify(result), 200


@app.route("/api/Recipes/<string:id>")
def get_recipe_by_id(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute("SELECT * FROM public.\"Recipes\" WHERE id = '{}'".format(id))
    recipe = cursor.fetchall()

    if len(recipe) == 0:
        cursor.execute("SELECT * FROM public.\"Recipes\" WHERE id = '{}'".format(id))
        recipe = cursor.fetchall()
        if len(recipe) == 0:
            return jsonify(error="Recipe id not found"), 404

    result = {}
    for i in range(len(recipe_cols)):
        result[recipe_cols[i]] = recipe[0][i]

    return jsonify(result), 200


@app.route("/api/Recipes/Plants/<string:id>")
def get_plants_for_recipe(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Plants".* FROM (public."plants_recipes" INNER JOIN public."Plants" ON public."plants_recipes".plant_id=public."Plants".id) WHERE public."plants_recipes".recipe_id = \'{}\''.format(
            id
        )
    )
    plants = cursor.fetchall()

    if len(plants) == 0:
        return jsonify(error="Recipe id not found"), 404

    result = {"plants": []}

    for plant in plants:
        plant_obj = {}
        for i in range(len(plant_cols)):
            plant_obj[plant_cols[i]] = plant[i]
        result["plants"].append(plant_obj)

    return jsonify(result), 200


@app.route("/api/Recipes/Cities/<string:id>")
def get_cities_for_recipe(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Cities".* FROM (public."recipes_cities" INNER JOIN public."Cities" ON public."recipes_cities".city_id=public."Cities".id) WHERE public."recipes_cities".recipe_id = \'{}\''.format(
            id
        )
    )
    cities = cursor.fetchall()

    if len(cities) == 0:
        return jsonify(error="Recipe id not found"), 404

    result = {"cities": []}

    for city in cities:
        city_obj = {}
        for i in range(len(city_cols)):
            city_obj[city_cols[i]] = city[i]
        result["cities"].append(city_obj)

    return jsonify(result), 200


@app.route("/api/Cities")
def get_all_cities():
    # db.session.query(Plants).all()
    cursor.execute('SELECT * FROM public."Cities"')
    cities = cursor.fetchall()

    result = {"cities": []}

    for city in cities:
        city_obj = {}
        for i in range(len(city_cols)):
            city_obj[city_cols[i]] = city[i]
        result["cities"].append(city_obj)

    return jsonify(result), 200


@app.route("/api/Cities/<string:id>")
def get_city_by_id(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute("SELECT * FROM public.\"Cities\" WHERE id = '{}'".format(id))
    city = cursor.fetchall()

    if len(city) == 0:
        cursor.execute("SELECT * FROM public.\"Cities\" WHERE id = '{}'".format(id))
        city = cursor.fetchall()
        if len(city) == 0:
            return jsonify(error="City id not found"), 404

    result = {}
    for i in range(len(city_cols)):
        result[city_cols[i]] = city[0][i]

    return jsonify(result), 200


@app.route("/api/Cities/Plants/<string:id>")
def get_plants_for_city(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Plants".* FROM (public."plants_cities" INNER JOIN public."Plants" ON public."plants_cities".plant_id=public."Plants".id) WHERE public."plants_cities".city_id = \'{}\''.format(
            id
        )
    )
    plants = cursor.fetchall()

    if len(plants) == 0:
        return jsonify(error="City id not found"), 404

    result = {"plants": []}

    for plant in plants:
        plant_obj = {}
        for i in range(len(plant_cols)):
            plant_obj[plant_cols[i]] = plant[i]
        result["plants"].append(plant_obj)

    return jsonify(result), 200


@app.route("/api/Cities/Recipes/<string:id>")
def get_recipes_for_city(id):
    # db.session.query(Plants).all()
    if not valid_uuid(id):
        return jsonify(error="Valid id not provided"), 404

    cursor.execute(
        'SELECT public."Recipes".* FROM (public."recipes_cities" INNER JOIN public."Recipes" ON public."recipes_cities".recipe_id=public."Recipes".id) WHERE public."recipes_cities".city_id = \'{}\''.format(
            id
        )
    )
    recipes = cursor.fetchall()

    if len(recipes) == 0:
        return jsonify(error="City id not found"), 404

    result = {"recipes": []}

    for recipe in recipes:
        recipe_obj = {}
        for i in range(len(recipe_cols)):
            recipe_obj[recipe_cols[i]] = recipe[i]
        result["recipes"].append(recipe_obj)

    return jsonify(result), 200


# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# manager.create_api(Person, methods=['GET', 'POST', 'DELETE'])
# manager.create_api(Plants, methods=['GET'])

# start the flask loop
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, threaded=True, debug=True)
