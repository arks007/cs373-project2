from main import valid_uuid
import unittest
import os
import uuid

class backend_tests(unittest.TestCase):
    def test_valid_uuid(self):
        x = 'cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a'
        assert valid_uuid(x)

    def test_valid_uuid2(self):
        x = ''
        assert not valid_uuid(x)

    def test_valid_uuid3(self):
        x = 'cb7a03a3-a71d-4c6c-9c6b-540c29c9ae4a'
        assert valid_uuid(x)
        y = x + 'g'
        assert not valid_uuid(y)
    



if __name__ == "__main__":
    unittest.main()
