# HomeFarmer

## The Big Picture
HomeFarmer is a web application that harnesses geographical, botanical, and nutritional data to aid users with recommendations for urban farming resources based on the principles of mutual aid.

## Website
https://homefarmer.me

## Team Members
| Name | EID | GitLab ID |
|---|---|---|
| Pranav Akinepalli | pa8789 | @pranav.akinepalli |
| Sameer Haniyur | svh353 | @shaniyur |
| Grant He | gh22593 | @grant-he |
| Sujoy Purkayastha | sp45569 | @arks007 |
| Jonathan Randall | jhr2388 | @jonathanhrandall |

## Completion Times
*Time is tracked in hours. Project leader's name is bolded.*
### Phase I:
| Name | Estimated | Actual |
| --- | --- | --- |
| Pranav Akinepalli | 30 | 50 |
| Sameer Haniyur | 24 | 12 |
| **Grant He** | 30 | 55 |
| Sujoy Purkayastha | 24 | 40 |
| Jonathan Randall | 30 | 55 |

### Phase II:
| Name | Estimated | Actual |
| --- | --- | --- |
| Pranav Akinepalli | 60 | 60 |
| Sameer Haniyur | 60 | 60 |
| Grant He | 50 | 50 |
| **Sujoy Purkayastha** | 60 | 60 |
| Jonathan Randall | 100 | 100 |

## Pipelines
https://gitlab.com/jonathanhrandall/cs373-project2/-/pipelines

## Git SHA
Phase I - 79ef90244bd60843c36f6ac293e8a1cbada74d59
Phase II - 449f9b822f71da06763047e6e35e436245dfd4ee