module.exports = {
  presets: [
    ['@babel/preset-env', {targets: {node: 'current'}}],
    '@babel/preset-typescript',
    '@babel/react'
  ],
  env: {
    "start": {
      "presets": [
        "react-hmre"
      ]
    },
    "test": {
      "plugins": ["transform-export-extensions"]
    }
  }
}