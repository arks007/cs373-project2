import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import HomeFarmerLogo from '../greenslogo.svg'
import Plants from '../pages/plants/Plants';

const useStyles = makeStyles(function () {
  return ({
    buttonPadding: {
      padding: '10px',
      margin: '20px',
    }
  });
});

export default function Home() {
  const classes = useStyles();
  return (
    <Grid container direction='row' justify='center' alignItems='center' style={{ margin: 0, width: '100%' }}>
      <Grid item>
        <Typography variant='h1' align='center'>
          Grow together.
          </Typography>
        <Typography variant='h6' align='center' >
          Change the way you eat.
          Learn how to farm in your own backyard.
          </Typography>
        <Grid container spacing={4} justify='space-evenly'>
          <Grid item>
            <Switch>
              <Route>
                <Button variant='contained' size='large' color='secondary' className={classes.buttonPadding} component={Link} to={'./plants'}>
                  Get started
                </Button>
              </Route>
              <Route path={'./plants'} children={<Plants />} />
            </Switch>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Image src={HomeFarmerLogo} alt='HomeFarmerLogo' fluid />
      </Grid>
    </Grid>
  );
}
