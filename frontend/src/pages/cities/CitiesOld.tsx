import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import MapSection from '../../components/Map' // import the map here
import { Route, Link, useRouteMatch, useParams, } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";
import Box from "@material-ui/core/Box";

//import React, { Component } from 'react';
// import axios from 'axios';
//
// import Posts from './components/Posts';

//import Pagination from './CitiesPagination';

const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 0
  },
  cardActionArea: {
    height: "100%"
  },
  marginAutoContainer: {
    width: "100%",
    height: 80,
    display: 'flex',
  },
  marginAutoItem: {
    margin: 'auto'
  },

}));

const cards = [
  {
    name: "Austin",
    pathname: "austin-tx",
    img: "https://i.imgur.com/o62YBci.jpg",
    state: "TX",
    lat: 30.27,
    lng: -97.74,
    tempMax: 75,
    tempMin: 69.01,
    pressure: 1020,
    humidity: 37
  },
  {
    name: "New York",
    pathname: "new-york-ny",
    img: "https://i.imgur.com/V8SWe2m.jpg",
    state: "NY",
    lat: 40.71,
    lng: -74.01,
    tempMax: 66.99,
    tempMin: 62.6,
    pressure: 1007,
    humidity: 68
  },
  {
    name: "Los Angeles",
    pathname: "los-angeles-ca",
    img: "https://i.imgur.com/8LLgwOH.jpg",
    state: "CA",
    lat: 34.05,
    lng: -118.24,
    tempMax: 89.01,
    tempMin: 68,
    pressure: 1014,
    humidity: 53
  }
];

// Individual CityCard
function CityCard({ city }: any) {
  let match = useRouteMatch();
  const classes = useStyles();
  return (
    <Link to={`${match.url}/${city.pathname}`} style={{ textDecoration: 'none' }}>

      <CardActionArea className={classes.cardActionArea}>
        <Card className={classes.card}>
          <CardMedia className={classes.cardMedia} image={city.img} title={city.name} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">{city.name}</Typography>
            <Typography>State: {city.state}</Typography>
            <Typography>Latitude: {city.lat}</Typography>
            <Typography>Longitude: {city.lng}</Typography>
            <Typography>Humidity: {city.humidity}</Typography>
            <Typography>Temperature Range: {Math.round(city.tempMin)} - {Math.round(city.tempMax)}°F</Typography>
          </CardContent>
        </Card>
      </CardActionArea>
    </Link>

  );
}

// Grid of all cities
function Cities() {
  const classes = useStyles();
  let match = useRouteMatch();

  return (
    <React.Fragment>
      <Route exact path="/cities">

        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">Cities</Typography>
        </Container>

        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {cards.map((city) => (
              <Grid item xs={12} sm={6} md={4}>
                <CityCard key={city.name} city={city} />
              </Grid>
            ))}
          </Grid>
        </Container>
        <Box display="flex" width={"100%"} height={80}>
          <Box m="auto">
            
          </Box>
        </Box>
      </Route>
      <Route path={`${match.path}/:cityId`} children={<City />} />
    </React.Fragment>
  );
}

const cityStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  cardContent: {
    flexGrow: 1
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));


// Display content for an individual city page
function City() {
  const classes = cityStyles();
  let { cityId }: any = useParams();
  let city = cards.find((card) => card.pathname === cityId);
  if (city) {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">
            {city.name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {city.state}
          </Typography>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <Card style={{ textAlign: "center" }}>
                <CardMedia className={classes.media} image={city.img} title={city.name} />
              </Card>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Card style={{ textAlign: "center" }}>
                <MapSection location={{ address: city.name, lat: city.lat, lng: city.lng }} zoomLevel={10} mapTypeId="hybrid" />
              </Card>
            </Grid>

            <Grid item xs={12} sm={6}>
              <CityTable key={city.name} city={city} />
            </Grid>

          </Grid>
        </Container>
      </div>
    );
  }
  return (
    <h3 className="text-center">City not found.</h3>
  );
}

const tableStyles = makeStyles({
  table: {
    maxWidth: 445,
  },
});

function CityTable({ city }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <colgroup>
          <col width="50%" />
          <col width="50%" />
        </colgroup>
        <TableBody>
          <TableRow>
            <TableCell align="center">State</TableCell>
            <TableCell align="center">{city.state}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Temperature Range</TableCell>
            <TableCell align="center">{Math.round(city.tempMin)} - {Math.round(city.tempMax)}°F</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Humidity</TableCell>
            <TableCell align="center">{city.humidity}%</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Pressure</TableCell>
            <TableCell align="center">{city.pressure / 1000} bars</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}






export default Cities;
