import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { Route, Link } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";
import City from "./City"
import Box from '@material-ui/core/Box';


const useStyles = makeStyles(theme => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 0
  },
  cardActionArea: {
    height: "100%"
  },

}));

// Individual CityCard
function CityCard({ instance }: any) {
  const classes = useStyles();
  return (
    <div>
          <Link to={`/cities/` + instance.id} style={{ textDecoration: 'none' }}>

            <CardActionArea className={classes.cardActionArea} >
              <Card className={classes.card}>
                <CardMedia className={classes.cardMedia} image={instance.image_url} title={instance.city_name} />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">{instance.city_name + ", " + instance.state_code}</Typography>
                  <Typography>State: {instance.state_name}</Typography>
                  <Typography>Latitude: {instance.latitude + "°"}</Typography>
                  <Typography>Longitude: {instance.longitude + "°"}</Typography>
                  <Typography>Humidity: {instance.humidity + "%"}</Typography>
                  <Typography>Temperature Range: {Math.round(instance.min_temp)} - {Math.round(instance.max_temp)}°F</Typography>
                </CardContent>
              </Card>
            </CardActionArea>
            <Box m={2} >
            </Box>
          </Link>

          <Route path={`/cities/:cityId`} children={<City />} />

    </div>
  );
}


export default CityCard;
