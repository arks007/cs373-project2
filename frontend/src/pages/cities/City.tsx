import React, {Component, useEffect, useState} from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import MapSection from '../../components/Map' // import the map here
import { Route, useParams, } from "react-router-dom";
import Box from '@material-ui/core/Box';


import Plant from "../plants/Plant"
import Recipe from "../recipes/Recipe"
import Instances from "../../components/Instances"
//import Recipe from "../recipes/Recipe"
import axios from 'axios';



const cityStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  cardContent: {
    flexGrow: 1
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));


// Display content for an individual city page
function City() {
  const classes = cityStyles();
  let { cityId }: any = useParams();

  const [cityObject, setCityObject] = useState({"id": "sammer city", "city_name": "", "state_name": "", "state_code": "", "latitude": "", "longitude": "", "min_temp": "", "max_temp": "", "humidity": "", "pressure": "", "image_url": ""});

  useEffect(() => {
    axios.get('http://0.0.0.0/api/Cities/' + cityId)
    .then(response => {
      console.log("USEEFECT: " + JSON.stringify(response.data));
      setCityObject(response.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    })
  }, [])

  if(cityObject.id === "sammer city"){
    return  (<h3 className="text-center">City not found.</h3>)
  }

  else{
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">
            {cityObject.city_name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {cityObject.state_name}
          </Typography>
        </Container>


        <Container  maxWidth="md">
          <Grid container spacing={4}>
              <Grid item xs >
                <Card style={{ textAlign: "center" }}>
                  <CardMedia className={classes.media} image={cityObject.image_url} title={cityObject.city_name} />
                </Card>
              </Grid>
              <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <MapSection location={{ address: cityObject.city_name, lat: cityObject.latitude, lng: cityObject.longitude }} zoomLevel={10} mapTypeId="hybrid" />
              </Card>
              </Grid>
          </Grid>
          <Grid container spacing={4} >
              <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CityTable key={cityObject.city_name} city={cityObject}/>
              </Card>
              </Grid>
          </Grid>

          <Box m={2} >
            <Typography  variant="h4" align="center" color="textPrimary" >
            Related
            </Typography>
          </Box>


          <RelatedCards id={cityObject.id}/>
        </Container>
      </div>
    );
  }
}


function CityTable({ city }: any) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <colgroup>
          <col width="50%" />
          <col width="50%" />
        </colgroup>
        <TableBody>
          <TableRow>
            <TableCell align="center">State</TableCell>
            <TableCell align="center">{city.state_name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Temperature Range</TableCell>
            <TableCell align="center">{Math.round(city.min_temp)} - {Math.round(city.max_temp)}°F</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Humidity</TableCell>
            <TableCell align="center">{city.humidity}%</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Pressure</TableCell>
            <TableCell align="center">{city.pressure / 1000} bars</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}


interface RelatedCardsProps { // Added this interface for props
  id: string,
}

export class RelatedCards extends Component <RelatedCardsProps>{
  state = {
    relatedPlants: [],
    relatedRecipes: [],
    loading: false
  };

  componentDidMount() {
    const {id}= this.props;
    const getInstances = async () => {
      this.setState({ loading: true });

      const results_1 = await axios.get("http://0.0.0.0/api/Cities/Plants/"+id);
      this.setState({ relatedPlants: results_1.data.plants });

      const results_2 = await axios.get("http://0.0.0.0/api/Cities/Recipes/"+id);
      this.setState({ relatedRecipes: results_2.data.recipes });

      this.setState({ loading: false });
    };

    getInstances();
  }

  render() {
    const { relatedPlants, relatedRecipes, loading } = this.state;




    return (

        <div>
            <Grid container spacing={4}>
                <Grid item xs >
                    <Instances instances={relatedPlants} card={"PlantCard"} loading={loading} />
                </Grid>
                <Grid item xs >
                    <Instances instances={relatedRecipes} card={"RecipeCard"} loading={loading} />
                </Grid>
            </Grid>
            <Route path={`/plants/:plantId`} children={<Plant />} />
            <Route path={`/recipes/:recipeId`} children={<Recipe />} />
        </div>


    )
  }
}



//<Route path={`/recipes/:recipeId`} children={<Recipe />} />
export default City;
