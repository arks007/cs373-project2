import React, {Component, useEffect, useState} from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';

import { Route,  useParams, } from "react-router-dom";


import Instances from "../../components/Instances"
import Plant from "../plants/Plant"
import City from "../cities/City"
import axios from 'axios';
import Box from '@material-ui/core/Box';

import { CardActionArea } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";

const recipeStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));


// Display content for an individual recipe page
function Recipe() {
  const classes = recipeStyles();
  let { recipeId }: any = useParams();

  const [recipeObject, setRecipeObject] = useState({"id": "sammer soup", "label": "", "yield": "", "ingredient_lines": "", "calories": "", "total_time": "", "source": "", "url": "", "image": ""});

  useEffect(() => {
    axios.get('http://0.0.0.0/api/Recipes/' + recipeId)
    .then(response => {
      console.log("USEEFECT: " + JSON.stringify(response.data));
      setRecipeObject(response.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    })
  }, [])

  if(recipeObject.id === "sammer soup"){
    return (
      <h3 className="text-center">Recipe not found.</h3>
    );
  }

  else{

    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h4" align="center" color="textPrimary">{recipeObject.label}</Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {recipeObject.source}
          </Typography>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs >
            <Card style={{ textAlign: "center" }}>
              <CardActionArea href={recipeObject.source}>
                <CardMedia className={classes.media}  image={recipeObject.image} title="Wikipedia" />
                <CardContent>{recipeObject.label}</CardContent>
              </CardActionArea>
            </Card>
            </Grid>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={recipeObject.url}>
                  <CardMedia className={classes.media}  image="https://images.unsplash.com/photo-1514411959691-a8f39b0ac8b8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2010&q=80" title="Wikipedia" />
                  <CardContent>Source</CardContent>
                </CardActionArea>
              </Card>
            </Grid>

          </Grid>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>

              <Typography variant="h5" align="center"  gutterBottom>Ingredients</Typography>
              <IngredientListTable key={recipeObject.label} recipe={recipeObject} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>Info</Typography>
              <RecipeTable key={recipeObject.label} recipe={recipeObject} />
            </Grid>
          </Grid>

          <Box m={2} >
            <Typography  variant="h4" align="center" color="textPrimary" >
            Related
            </Typography>
          </Box>

          <RelatedCards id={recipeObject.id}/>
        </Container>
      </div>
    );
  }
}

const tableStyles = makeStyles({
  table: {

  },
});

function RecipeTable({ recipe }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="recipe table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Time</TableCell>
            <TableCell align="center">{recipe.total_time} minutes</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Yield</TableCell>
            <TableCell align="center">{recipe.yield} servings</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Calories</TableCell>
            <TableCell align="center">{Math.floor(recipe.calories)}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Source</TableCell>
            <TableCell align="center"><a href={recipe.url}>{recipe.source}</a></TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

function IngredientListTable({ recipe }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="ingredient list table">
        <TableBody>
          {Object.values(recipe.ingredient_lines).map((value: any) => (
            <TableRow>
              <TableCell align="center">{value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

// function NutritionTable({ recipe }: any) {
//   const classes = tableStyles();
//   return (
//     <TableContainer component={Paper}>
//       <Table className={classes.table} aria-label="nutrition table">
//         <TableBody>
//           {Object.values(recipe.nutrients).map((value: any) => (
//             <TableRow>
//               <TableCell align="center"> {value.label}</TableCell>
//               <TableCell align="center"> {Math.ceil(value.quantity * 100) / 100 + " " + value.unit}</TableCell>
//             </TableRow>
//           ))}
//         </TableBody>
//       </Table>
//     </TableContainer>
//   );
// }

interface RelatedCardsProps { // Added this interface for props
  id: string,
}

export class RelatedCards extends Component <RelatedCardsProps>{
  state = {
    relatedPlants: [],
    relatedCities: [],
    loading: false
  };

  componentDidMount() {
    const {id}= this.props;
    const getInstances = async () => {
      this.setState({ loading: true });

      const results_1 = await axios.get("http://0.0.0.0/api/Recipes/Plants/"+id);
      this.setState({ relatedPlants: results_1.data.plants });

      const results_2 = await axios.get("http://0.0.0.0/api/Recipes/Cities/"+id);
      this.setState({ relatedCities: results_2.data.cities });

      this.setState({ loading: false });
    };

    getInstances();
  }

  render() {
    const { relatedPlants, relatedCities, loading } = this.state;




    return (

        <div>
            <Grid container spacing={4}>
                <Grid item xs >
                    <Instances instances={relatedPlants} card={"PlantCard"} loading={loading} />
                </Grid>
                <Grid item xs >
                    <Instances instances={relatedCities} card={"CityCard"} loading={loading} />
                </Grid>
            </Grid>
            <Route path={`/plants/:plantId`} children={<Plant />} />
            <Route path={`/cities/:cityId`} children={<City />} />
        </div>


    )
  }
}


export default Recipe;
