import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";
import CookTimeIcon from '@material-ui/icons/AccessTime';
import ServingsIcon from '@material-ui/icons/People';
import CaloriesIcon from '@material-ui/icons/Fastfood';
import CardActions from "@material-ui/core/CardActions";
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';


const useStyles = makeStyles(theme => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 0
  },
  cardActionArea: {
    height: "100%"
  },

}));
// Individual RecipeCard
function RecipeCard({ instance }: any) {
  const classes = useStyles();
  return (
    <Link to={'/recipes/' + instance.id} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea} >
        <Card className={classes.card}>

          <CardMedia className={classes.cardMedia} image={instance.image} title={instance.label} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">{instance.label}</Typography>
          </CardContent>
          <CardActions>
            <Grid container spacing={0} alignItems={"baseline"}>

              <Grid item xs={4}>
                <Paper elevation={0}>
                  <Typography gutterBottom align="center" component="h2">Time</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs>
                      <CookTimeIcon />
                    </Grid>
                    <Grid item xs>
                      <Typography>{instance.total_time}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper elevation={0}>
                  <Typography gutterBottom align="center" component="h2">Servings</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12} sm={6}>
                      <ServingsIcon />
                    </Grid>
                    <Grid item >
                      <Typography>{instance.yield}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper elevation={0} >
                  <Typography gutterBottom align="center" component="h2">Calories</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12} sm={6}>
                      <CaloriesIcon />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography>{Math.floor(instance.calories)}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>

          </CardActions>
        </Card>
      </CardActionArea>
      <Box m={2} >
      </Box>

    </Link>
  );
}


export default RecipeCard;
