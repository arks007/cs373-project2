import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import CookTimeIcon from '@material-ui/icons/AccessTime';
import ServingsIcon from '@material-ui/icons/People';
import CaloriesIcon from '@material-ui/icons/Fastfood';
import { Route, Link, useRouteMatch, useParams, } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";


const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  },
  paper: {
    padding: theme.spacing(0),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardActionArea: {
    height: "100%"
  },
}));

const cards = [
  {
    name: "Grilled Marinated Heart of Palm Tacos With Spicy Cabbage Slaw",
    pathname: "grilled-marinated-heart-of-palm-tacos-with-spicy-cabbage-slaw",
    img: "https://www.edamam.com/web-img/6ed/6ed783e1406c3d8a8765071f685d0e0b.jpg",
    time: 30.0,
    calories: 1378.223788646228,
    yield: 3.0,
    ingredients: [
      {
        "text": "2 teaspoons lime juice from 1 lime, plus extra lime wedges for serving",
        "weight": 10.266666667187403,
        "image": "https://www.edamam.com/food-img/8f0/8f0c10eb3dbf476a05e61018e76ea220.jpg"
      },
      {
        "text": "1 medium clove garlic, minced (about 1 teaspoon)",
        "weight": 2.8,
        "image": "https://www.edamam.com/food-img/6ee/6ee142951f48aaf94f4312409f8d133d.jpg"
      },
      {
        "text": "1 teaspoon minced fresh thyme or oregano leaves",
        "weight": 0.8,
        "image": "https://www.edamam.com/food-img/3e7/3e7cf3c8d767a90b906447f5e74059f7.jpg"
      },
      {
        "text": "1 tablespoon olive oil",
        "weight": 13.5,
        "image": null
      },
      {
        "text": "Kosher salt and freshly ground black pepper",
        "weight": 8.363657081278417,
        "image": "https://www.edamam.com/food-img/694/6943ea510918c6025795e8dc6e6eaaeb.jpg"
      },
      {
        "text": "Kosher salt and freshly ground black pepper",
        "weight": 4.181828540639208,
        "image": "https://www.edamam.com/food-img/c6e/c6e5c3bd8d3bc15175d9766971a4d1b2.jpg"
      },
      {
        "text": "1 (15 ounce) can of hearts of palm, drained, or 1/2 pound fresh hearts of palm",
        "weight": 425.242846875,
        "image": "https://www.edamam.com/food-img/dd8/dd84c7e7bcb19f68e4568d196d1638dc.jpg"
      },
      {
        "text": "1/2 teaspoon vegetable oil",
        "weight": 2.333333333491132,
        "image": "https://www.edamam.com/food-img/6e5/6e51a63a6300a8ea1b4c4cc68dfaba33.jpg"
      },
      {
        "text": "12 corn tortillas",
        "weight": 288.0,
        "image": "https://www.edamam.com/food-img/b8a/b8ad23dcc06f2324f944e47eb579d644.jpg"
      },
      {
        "text": "1 cup homemade or store-bought salsa verde, or other salsa",
        "weight": 240.00000000405768,
        "image": "https://www.edamam.com/food-img/205/205d9e349c3b7e60d4886c5ab4e27a92.jpg"
      },
      {
        "text": "Sliced white onion (for serving)",
        "weight": 32.0,
        "image": "https://www.edamam.com/food-img/205/205e6bf2399b85d34741892ef91cc603.jpg"
      },
      {
        "text": "Picked cilantro (for serving)",
        "weight": 13.939428468797361,
        "image": "https://www.edamam.com/food-img/d57/d57e375b6ff99a90c7ee2b1990a1af36.jpg"
      },
      {
        "text": "2 cups Spicy Cabbage Slaw",
        "weight": 178.0,
        "image": "https://www.edamam.com/food-img/cb1/cb1411c925c19de26620e63cb90d0e14.jpg"
      },
      {
        "text": "1 ripe avocado, sliced or mashed (for serving, optional)",
        "weight": 201.0,
        "image": "https://www.edamam.com/food-img/984/984a707ea8e9c6bf5f6498970a9e6d9d.jpg"
      }
    ],
    ingredientList: [
      "2 teaspoons lime juice from 1 lime, plus extra lime wedges for serving",
      "1 medium clove garlic, minced (about 1 teaspoon)",
      "1 teaspoon minced fresh thyme or oregano leaves",
      "1 tablespoon olive oil",
      "Kosher salt and freshly ground black pepper",
      "1 (15 ounce) can of hearts of palm, drained, or 1/2 pound fresh hearts of palm",
      "1/2 teaspoon vegetable oil",
      "12 corn tortillas",
      "1 cup homemade or store-bought salsa verde, or other salsa",
      "Sliced white onion (for serving)",
      "Picked cilantro (for serving)",
      "2 cups Spicy Cabbage Slaw",
      "1 ripe avocado, sliced or mashed (for serving, optional)"
    ],
    instructionsSource: "Serious Eats",
    instructionsURL: "http://www.seriouseats.com/recipes/2013/02/grilled-marinated-heart-of-palm-tacos-recipe.html",
    nutrients: {
      "ENERC_KCAL": {
        "label": "Energy",
        "quantity": 1378.223788646228,
        "unit": "kcal"
      },
      "FAT": {
        "label": "Fat",
        "quantity": 58.73387828928186,
        "unit": "g"
      },
      "FASAT": {
        "label": "Saturated",
        "quantity": 8.287890940885813,
        "unit": "g"
      },
      "FATRN": {
        "label": "Trans",
        "quantity": 0.017896666667876985,
        "unit": "g"
      },
      "FAMS": {
        "label": "Monounsaturated",
        "quantity": 33.76190960693275,
        "unit": "g"
      },
      "FAPU": {
        "label": "Polyunsaturated",
        "quantity": 10.512028637604365,
        "unit": "g"
      },
      "CHOCDF": {
        "label": "Carbs",
        "quantity": 199.10310923580388,
        "unit": "g"
      },
      "FIBTG": {
        "label": "Fiber",
        "quantity": 53.031001609653885,
        "unit": "g"
      },
      "SUGAR": {
        "label": "Sugars",
        "quantity": 19.663343397156112,
        "unit": "g"
      },
      "PROCNT": {
        "label": "Protein",
        "quantity": 37.49160155305584,
        "unit": "g"
      },
      "CHOLE": {
        "label": "Cholesterol",
        "quantity": 0.0,
        "unit": "mg"
      },
      "NA": {
        "label": "Sodium",
        "quantity": 3436.7963638489646,
        "unit": "mg"
      },
      "CA": {
        "label": "Calcium",
        "quantity": 641.9461020303975,
        "unit": "mg"
      },
      "MG": {
        "label": "Magnesium",
        "quantity": 510.97879335302633,
        "unit": "mg"
      },
      "K": {
        "label": "Potassium",
        "quantity": 3390.5777626073977,
        "unit": "mg"
      },
      "FE": {
        "label": "Iron",
        "quantity": 21.346624542408122,
        "unit": "mg"
      },
      "ZN": {
        "label": "Zinc",
        "quantity": 11.098926974383987,
        "unit": "mg"
      },
      "P": {
        "label": "Phosphorus",
        "quantity": 1459.0753985630522,
        "unit": "mg"
      },
      "VITA_RAE": {
        "label": "Vitamin A",
        "quantity": 99.58430097960981,
        "unit": "µg"
      },
      "VITC": {
        "label": "Vitamin C",
        "quantity": 159.7282305903556,
        "unit": "mg"
      },
      "THIA": {
        "label": "Thiamin (B1)",
        "quantity": 0.708273171722898,
        "unit": "mg"
      },
      "RIBF": {
        "label": "Riboflavin (B2)",
        "quantity": 0.8956255882128912,
        "unit": "mg"
      },
      "NIA": {
        "label": "Niacin (B3)",
        "quantity": 14.247425440938596,
        "unit": "mg"
      },
      "VITB6A": {
        "label": "Vitamin B6",
        "quantity": 1.7733676291211675,
        "unit": "mg"
      },
      "FOLDFE": {
        "label": "Folate equivalent (total)",
        "quantity": 455.6987334508565,
        "unit": "µg"
      },
      "FOLFD": {
        "label": "Folate (food)",
        "quantity": 455.6987334508565,
        "unit": "µg"
      },
      "FOLAC": {
        "label": "Folic acid",
        "quantity": 0.0,
        "unit": "µg"
      },
      "VITB12": {
        "label": "Vitamin B12",
        "quantity": 0.0,
        "unit": "µg"
      },
      "VITD": {
        "label": "Vitamin D",
        "quantity": 0.0,
        "unit": "µg"
      },
      "TOCPHA": {
        "label": "Vitamin E",
        "quantity": 9.13522006192891,
        "unit": "mg"
      },
      "VITK1": {
        "label": "Vitamin K",
        "quantity": 268.5520815748532,
        "unit": "µg"
      },
      "WATER": {
        "label": "Water",
        "quantity": 1095.2249773792635,
        "unit": "g"
      }
    }
  },
  {
    name: "Pasta With Snap Peas, Garlic, Lemon Zest, and Black Pepper",
    pathname: "pasta-with-snap-peas-garlic-lemon-zest-and-black-pepper",
    img: "https://www.edamam.com/web-img/22c/22ca426327c5fc683bb2dbf3d159329d.jpg",
    time: 20.0,
    calories: 1544.7649445929776,
    yield: 3.0,
    ingredients: [
      {
        "text": "1 pound snap peas, trimmed",
        "weight": 453.59237,
        "image": "https://www.edamam.com/food-img/d69/d6979db01580bcc6b32ad9a83cb5148c.jpg"
      },
      {
        "text": "Kosher salt",
        "weight": 4.611720392499429,
        "image": "https://www.edamam.com/food-img/694/6943ea510918c6025795e8dc6e6eaaeb.jpg"
      },
      {
        "text": "1/2 teaspoon corn starch",
        "weight": 1.33333333340096,
        "image": "https://www.edamam.com/food-img/f9b/f9b74d9495b40c0aea955c37a1fc39dc.jpg"
      },
      {
        "text": "1/2 pound ridged pasta such as cavatappi, penne rigate, or ziti",
        "weight": 226.796185,
        "image": null
      },
      {
        "text": "1/4 cup extra virgin olive oil",
        "weight": 54.0,
        "image": null
      },
      {
        "text": "4 cloves garlic, finely sliced",
        "weight": 12.0,
        "image": "https://www.edamam.com/food-img/6ee/6ee142951f48aaf94f4312409f8d133d.jpg"
      },
      {
        "text": "Pinch red pepper flakes",
        "weight": 0.04817708339442312,
        "image": "https://www.edamam.com/food-img/6cb/6cb8e4510251a322178f6e191b3a7b1b.jpeg"
      },
      {
        "text": "Freshly ground black pepper",
        "weight": 2.3058601962497147,
        "image": "https://www.edamam.com/food-img/c6e/c6e5c3bd8d3bc15175d9766971a4d1b2.jpg"
      },
      {
        "text": "1 tablespoon juice and 1 teaspoon zest from 1 lemon",
        "weight": 13.249999999775984,
        "image": "https://www.edamam.com/food-img/70a/70acba3d4c734d7c70ef4efeed85dc8f.jpg"
      },
      {
        "text": "2 tablespoons minced fresh parsley leaves",
        "weight": 7.6,
        "image": "https://www.edamam.com/food-img/46a/46a132e96626d7989b4d6ed8c91f4da0.jpg"
      }
    ],
    ingredientList: [
      "3 tablespoons olive oil, divided",
      "3 cups broccoli florets, stems trimmed",
      "3 cups cauliflower florets, stems trimmed",
      "Kosher salt and freshly ground black pepper",
      "1 red onion, thinly sliced",
      "1 tablespoon curry powder",
      "½ tablespoon ground cumin",
      "1 cup quinoa, pre-rinsed or washed",
      "2 cups homemade vegetable stock or store-bought low-sodium vegetable broth",
      "2 1/2 cups lacinato or curly kale, leaves removed from the stalks and cut into 1-inch ribbons",
      "1 tablespoon fresh juice from 1 lime",
      "½ cup roughly chopped fresh cilantro",
      "½ cup microgreens, such as sunflower (optional)"
    ],
    instructionsSource: "Serious Eats",
    instructionsURL: "http://www.seriouseats.com/recipes/2013/02/pasta-with-snap-peas-garlic-lemon-zest-black-pepper-recipe.html",
    nutrients: {
      "ENERC_KCAL": {
        "label": "Energy",
        "quantity": 1249.6736428382685,
        "unit": "kcal"
      },
      "FAT": {
        "label": "Fat",
        "quantity": 55.00354052233802,
        "unit": "g"
      },
      "FASAT": {
        "label": "Saturated",
        "quantity": 7.663002880639754,
        "unit": "g"
      },
      "FATRN": {
        "label": "Trans",
        "quantity": 0.0,
        "unit": "g"
      },
      "FAMS": {
        "label": "Monounsaturated",
        "quantity": 33.6394171442198,
        "unit": "g"
      },
      "FAPU": {
        "label": "Polyunsaturated",
        "quantity": 10.90471047173945,
        "unit": "g"
      },
      "CHOCDF": {
        "label": "Carbs",
        "quantity": 161.7131368184783,
        "unit": "g"
      },
      "FIBTG": {
        "label": "Fiber",
        "quantity": 27.744366505332756,
        "unit": "g"
      },
      "SUGAR": {
        "label": "Sugars",
        "quantity": 13.185747799043972,
        "unit": "g"
      },
      "PROCNT": {
        "label": "Protein",
        "quantity": 42.54064922825307,
        "unit": "g"
      },
      "CHOLE": {
        "label": "Cholesterol",
        "quantity": 0.0,
        "unit": "mg"
      },
      "NA": {
        "label": "Sodium",
        "quantity": 3291.819098334189,
        "unit": "mg"
      },
      "CA": {
        "label": "Calcium",
        "quantity": 456.4839201653763,
        "unit": "mg"
      },
      "MG": {
        "label": "Magnesium",
        "quantity": 515.2492724686267,
        "unit": "mg"
      },
      "K": {
        "label": "Potassium",
        "quantity": 3263.980032662043,
        "unit": "mg"
      },
      "FE": {
        "label": "Iron",
        "quantity": 16.18646259727861,
        "unit": "mg"
      },
      "ZN": {
        "label": "Zinc",
        "quantity": 8.204143459505763,
        "unit": "mg"
      },
      "P": {
        "label": "Phosphorus",
        "quantity": 1197.9220557091048,
        "unit": "mg"
      },
      "VITA_RAE": {
        "label": "Vitamin A",
        "quantity": 568.6449086778604,
        "unit": "µg"
      },
      "VITC": {
        "label": "Vitamin C",
        "quantity": 419.61053405784435,
        "unit": "mg"
      },
      "THIA": {
        "label": "Thiamin (B1)",
        "quantity": 1.0732122843997127,
        "unit": "mg"
      },
      "RIBF": {
        "label": "Riboflavin (B2)",
        "quantity": 1.1467638589598057,
        "unit": "mg"
      },
      "NIA": {
        "label": "Niacin (B3)",
        "quantity": 6.774568895618074,
        "unit": "mg"
      },
      "VITB6A": {
        "label": "Vitamin B6",
        "quantity": 2.082514042199584,
        "unit": "mg"
      },
      "FOLDFE": {
        "label": "Folate equivalent (total)",
        "quantity": 746.2721429603438,
        "unit": "µg"
      },
      "FOLFD": {
        "label": "Folate (food)",
        "quantity": 746.2721429603438,
        "unit": "µg"
      },
      "FOLAC": {
        "label": "Folic acid",
        "quantity": 0.0,
        "unit": "µg"
      },
      "VITB12": {
        "label": "Vitamin B12",
        "quantity": 2.77394E-4,
        "unit": "µg"
      },
      "VITD": {
        "label": "Vitamin D",
        "quantity": 0.00166164,
        "unit": "µg"
      },
      "TOCPHA": {
        "label": "Vitamin E",
        "quantity": 12.878941671477884,
        "unit": "mg"
      },
      "VITK1": {
        "label": "Vitamin K",
        "quantity": 406.81765204306976,
        "unit": "µg"
      },
      "WATER": {
        "label": "Water",
        "quantity": 1145.051382225657,
        "unit": "g"
      }
    }
  },
  {
    name: "Vegan Quinoa, Broccoli, and Kale Curry",
    pathname: "vegan-quinoa-broccoli-and-kale-curry",
    img: "https://www.edamam.com/web-img/667/667d447cc92b50f83b39767871acf2a1.jpg",
    time: 30.0,
    calories: 1249.6736428382685,
    yield: 4.0,
    ingredients: [
      {
        "text": "3 tablespoons olive oil, divided",
        "weight": 40.5,
        "image": null
      },
      {
        "text": "3 cups broccoli florets, stems trimmed",
        "weight": 213.0,
        "image": "https://www.edamam.com/food-img/b11/b11b34a0dd535bf7aabeeb5413f24954.jpeg"
      },
      {
        "text": "3 cups cauliflower florets, stems trimmed",
        "weight": 321.0,
        "image": "https://www.edamam.com/food-img/ca2/ca217d31067dffd35ce1215e7f336bd8.jpg"
      },
      {
        "text": "Kosher salt and freshly ground black pepper",
        "weight": 8.467859999994316,
        "image": "https://www.edamam.com/food-img/694/6943ea510918c6025795e8dc6e6eaaeb.jpg"
      },
      {
        "text": "Kosher salt and freshly ground black pepper",
        "weight": 4.233929999997158,
        "image": "https://www.edamam.com/food-img/c6e/c6e5c3bd8d3bc15175d9766971a4d1b2.jpg"
      },
      {
        "text": "1 red onion, thinly sliced",
        "weight": 125.0,
        "image": "https://www.edamam.com/food-img/205/205e6bf2399b85d34741892ef91cc603.jpg"
      },
      {
        "text": "1 tablespoon curry powder",
        "weight": 6.3,
        "image": "https://www.edamam.com/food-img/9ce/9ce02a2887385fd2adaec8dd8adcf9c5.jpg"
      },
      {
        "text": "½ tablespoon ground cumin",
        "weight": 3.0,
        "image": "https://www.edamam.com/food-img/07e/07e2a4eb77ce46591033846504817d35.jpg"
      },
      {
        "text": "1 cup quinoa, pre-rinsed or washed",
        "weight": 170.0,
        "image": "https://www.edamam.com/food-img/b62/b622239a214b3bd9f63c8e93e6e9cde6.jpg"
      },
      {
        "text": "2 cups homemade vegetable stock or store-bought low-sodium vegetable broth",
        "weight": 454.0,
        "image": null
      },
      {
        "text": "2 1/2 cups lacinato or curly kale, leaves removed from the stalks and cut into 1-inch ribbons",
        "weight": 40.0,
        "image": "https://www.edamam.com/food-img/8e9/8e9c5417fa012e5ca068fddc5f8d3d6c.jpg"
      },
      {
        "text": "1 tablespoon fresh juice from 1 lime",
        "weight": 14.009999999052532,
        "image": "https://www.edamam.com/food-img/48a/48a123c9576647c4ada6a41df5eeb22a.jpg"
      },
      {
        "text": "½ cup roughly chopped fresh cilantro",
        "weight": 8.0,
        "image": "https://www.edamam.com/food-img/d57/d57e375b6ff99a90c7ee2b1990a1af36.jpg"
      },
      {
        "text": "½ cup microgreens, such as sunflower (optional)",
        "weight": 16.5,
        "image": "https://www.edamam.com/food-img/c76/c76a72667fcb45bc354eabb4ad7876a7.jpg"
      }
    ],
    ingredientList: [
      "3 tablespoons olive oil, divided",
      "3 cups broccoli florets, stems trimmed",
      "3 cups cauliflower florets, stems trimmed",
      "Kosher salt and freshly ground black pepper",
      "1 red onion, thinly sliced",
      "1 tablespoon curry powder",
      "½ tablespoon ground cumin",
      "1 cup quinoa, pre-rinsed or washed",
      "2 cups homemade vegetable stock or store-bought low-sodium vegetable broth",
      "2 1/2 cups lacinato or curly kale, leaves removed from the stalks and cut into 1-inch ribbons",
      "1 tablespoon fresh juice from 1 lime",
      "½ cup roughly chopped fresh cilantro",
      "½ cup microgreens, such as sunflower (optional)"
    ],
    instructionsSource: "Serious Eats",
    instructionsURL: "http://www.seriouseats.com/recipes/2014/04/vegan-curried-quinoa-with-broccoli-and-kale.html",
    nutrients: {
      "ENERC_KCAL": {
        "label": "Energy",
        "quantity": 1249.6736428382685,
        "unit": "kcal"
      },
      "FAT": {
        "label": "Fat",
        "quantity": 55.00354052233802,
        "unit": "g"
      },
      "FASAT": {
        "label": "Saturated",
        "quantity": 7.663002880639754,
        "unit": "g"
      },
      "FATRN": {
        "label": "Trans",
        "quantity": 0.0,
        "unit": "g"
      },
      "FAMS": {
        "label": "Monounsaturated",
        "quantity": 33.6394171442198,
        "unit": "g"
      },
      "FAPU": {
        "label": "Polyunsaturated",
        "quantity": 10.90471047173945,
        "unit": "g"
      },
      "CHOCDF": {
        "label": "Carbs",
        "quantity": 161.7131368184783,
        "unit": "g"
      },
      "FIBTG": {
        "label": "Fiber",
        "quantity": 27.744366505332756,
        "unit": "g"
      },
      "SUGAR": {
        "label": "Sugars",
        "quantity": 13.185747799043972,
        "unit": "g"
      },
      "PROCNT": {
        "label": "Protein",
        "quantity": 42.54064922825307,
        "unit": "g"
      },
      "CHOLE": {
        "label": "Cholesterol",
        "quantity": 0.0,
        "unit": "mg"
      },
      "NA": {
        "label": "Sodium",
        "quantity": 3291.819098334189,
        "unit": "mg"
      },
      "CA": {
        "label": "Calcium",
        "quantity": 456.4839201653763,
        "unit": "mg"
      },
      "MG": {
        "label": "Magnesium",
        "quantity": 515.2492724686267,
        "unit": "mg"
      },
      "K": {
        "label": "Potassium",
        "quantity": 3263.980032662043,
        "unit": "mg"
      },
      "FE": {
        "label": "Iron",
        "quantity": 16.18646259727861,
        "unit": "mg"
      },
      "ZN": {
        "label": "Zinc",
        "quantity": 8.204143459505763,
        "unit": "mg"
      },
      "P": {
        "label": "Phosphorus",
        "quantity": 1197.9220557091048,
        "unit": "mg"
      },
      "VITA_RAE": {
        "label": "Vitamin A",
        "quantity": 568.6449086778604,
        "unit": "µg"
      },
      "VITC": {
        "label": "Vitamin C",
        "quantity": 419.61053405784435,
        "unit": "mg"
      },
      "THIA": {
        "label": "Thiamin (B1)",
        "quantity": 1.0732122843997127,
        "unit": "mg"
      },
      "RIBF": {
        "label": "Riboflavin (B2)",
        "quantity": 1.1467638589598057,
        "unit": "mg"
      },
      "NIA": {
        "label": "Niacin (B3)",
        "quantity": 6.774568895618074,
        "unit": "mg"
      },
      "VITB6A": {
        "label": "Vitamin B6",
        "quantity": 2.082514042199584,
        "unit": "mg"
      },
      "FOLDFE": {
        "label": "Folate equivalent (total)",
        "quantity": 746.2721429603438,
        "unit": "µg"
      },
      "FOLFD": {
        "label": "Folate (food)",
        "quantity": 746.2721429603438,
        "unit": "µg"
      },
      "FOLAC": {
        "label": "Folic acid",
        "quantity": 0.0,
        "unit": "µg"
      },
      "VITB12": {
        "label": "Vitamin B12",
        "quantity": 2.77394E-4,
        "unit": "µg"
      },
      "VITD": {
        "label": "Vitamin D",
        "quantity": 0.00166164,
        "unit": "µg"
      },
      "TOCPHA": {
        "label": "Vitamin E",
        "quantity": 12.878941671477884,
        "unit": "mg"
      },
      "VITK1": {
        "label": "Vitamin K",
        "quantity": 406.81765204306976,
        "unit": "µg"
      },
      "WATER": {
        "label": "Water",
        "quantity": 1145.051382225657,
        "unit": "g"
      }
    }
  }
];

// Individual RecipeCard
function RecipeCard({ recipe }: any) {
  const classes = useStyles();
  let match = useRouteMatch();
  return (
    <Link to={`${match.url}/${recipe.pathname}`} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea}>
        <Card className={classes.card}>

          <CardMedia className={classes.cardMedia} image={recipe.img} title={recipe.name} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h6" component="h2">{recipe.name}</Typography>
          </CardContent>
          <CardActions>
            <Grid container spacing={0} alignItems={"baseline"}>

              <Grid item xs={4}>
                <Paper className={classes.paper} elevation={0}>
                  <Typography gutterBottom component="h2">Time</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12} sm={6}>
                      <CookTimeIcon />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography>{recipe.time}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper className={classes.paper} elevation={0}>
                  <Typography gutterBottom component="h2">Servings</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12} sm={6}>
                      <ServingsIcon />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography>{recipe.yield}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper className={classes.paper} elevation={0} >
                  <Typography gutterBottom component="h2">Calories</Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12} sm={6}>
                      <CaloriesIcon />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography>{Math.floor(recipe.calories)}</Typography>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>

          </CardActions>
        </Card>
      </CardActionArea>
    </Link>
  );
}

export default function Recipes() {
  const classes = useStyles();
  let match = useRouteMatch();
  return (
    <React.Fragment>
      <Route exact path="/recipes">
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">Recipes</Typography>
        </Container>

        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {cards.map(recipe => (
              <Grid item xs={12} sm={6} md={4}>
                <RecipeCard key={recipe.name} recipe={recipe} />
              </Grid>
            ))}
          </Grid>
        </Container>
      </Route>
      <Route path={`${match.path}/:recipeId`} children={<Recipe />} />
    </React.Fragment>

  );
}

const recipeStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));

// Display content for an individual recipe page
function Recipe() {
  const classes = recipeStyles();
  let { recipeId }: any = useParams();
  let recipe = cards.find((card) => card.pathname === recipeId);
  if (recipe) {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h4" align="center" color="textPrimary">{recipe.name}</Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {recipe.instructionsSource}
          </Typography>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <Card style={{ textAlign: "center" }}>
                <CardMedia className={classes.media} image={recipe.img} title={recipe.name} />
              </Card>
            </Grid>
            <Grid item xs={12} sm={6}>
              <RecipeTable key={recipe.name} recipe={recipe} />
            </Grid>
          </Grid>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>Ingredients</Typography>
              <IngredientListTable key={recipe.name} recipe={recipe} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" align="center" gutterBottom>Nutrition</Typography>
              <NutritionTable key={recipe.name} recipe={recipe} />
            </Grid>
          </Grid>
        </Container>


      </div>
    );
  }
  return (
    <h3 className="text-center">Recipe not found.</h3>
  );
}

const tableStyles = makeStyles({
  table: {

  },
});

function RecipeTable({ recipe }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="recipe table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Time</TableCell>
            <TableCell align="center">{recipe.time} minutes</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Yield</TableCell>
            <TableCell align="center">{recipe.yield} servings</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Calories</TableCell>
            <TableCell align="center">{Math.floor(recipe.calories)}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Source</TableCell>
            <TableCell align="center"><a href={recipe.instructionsURL}>{recipe.instructionsSource}</a></TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

function IngredientListTable({ recipe }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="ingredient list table">
        <TableBody>
          {Object.values(recipe.ingredientList).map((value: any) => (
            <TableRow>
              <TableCell align="center">{value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

function NutritionTable({ recipe }: any) {
  const classes = tableStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="nutrition table">
        <TableBody>
          {Object.values(recipe.nutrients).map((value: any) => (
            <TableRow>
              <TableCell align="center"> {value.label}</TableCell>
              <TableCell align="center"> {Math.ceil(value.quantity * 100) / 100 + " " + value.unit}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
