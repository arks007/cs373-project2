import React, { Component } from 'react';
import axios from 'axios';
import { Route } from "react-router-dom";

import Pagination from '../../components/MyPagination';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import PlantCard from './PlantCard';
import Plant from './Plant';
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";


export class PlantGrid extends Component {
  state = {
    instances: [],
    loading: false,
    currentPage: 1,
    instancesPerPage: 3
  };

  componentDidMount() {
    const getInstances = async () => {
      this.setState({ loading: true });
      //const results = await axios.get('https://jsonplaceholder.typicode.com/posts');
      const fetch = await axios.get('http://0.0.0.0:80/api/Plants');
      const results = fetch.data.plants;
      this.setState({ instances: results });
      this.setState({ loading: false });
    };

    getInstances();
  }

  render() {
    const { currentPage, instancesPerPage, instances, loading } = this.state;

    const indexOfLastInstance = currentPage * instancesPerPage;
    const indexOfFirstInstance = indexOfLastInstance - instancesPerPage;
    const currentInstances = instances.slice(indexOfFirstInstance, indexOfLastInstance);

    const paginate = (pageNum:any) => this.setState({ currentPage: pageNum });


    return (

      <div>
        <Route exact path="/plants">
          <div className="container">

              <Container maxWidth="sm">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>Plants</Typography>
                <Typography/>
              </Container>

              <Instances instances={currentInstances} loading={loading} />


              <Typography  gutterBottom/>

              <Box
                display="flex"
                width={"100%"} height={80}
              >
                <Box m="auto">
                  <Pagination instancesPerPage={instancesPerPage} totalInstances={instances.length} paginate={paginate}  />
                </Box>
              </Box>

          </div>
        </Route>
        <Route path={`/plants/:plantId`} children={<Plant />} />
      </div>


    )
  }
}



interface InstancesProps { // Added this interface for props
  instances: any,
  loading: boolean,
}

export class Instances extends Component <InstancesProps>{
    render() {
        const { instances, loading } = this.props;

        if (loading) {
            return <h2>Loading...</h2>
        }

        return (

            <div>
                <Container  maxWidth="md">
                  <Grid container spacing={4}>
                    {instances.map( (instance:any) => (
                      <Grid item xs={12} sm={6} md={4}>
                        <PlantCard key={instance.id} instance={instance} />
                      </Grid>
                    ))}
                  </Grid>
                </Container>

            </div>
        )
    }
}

export default PlantGrid;
