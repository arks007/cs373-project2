import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";
import Box from '@material-ui/core/Box';


const useStyles = makeStyles(theme => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 0
  },
  cardActionArea: {
    height: "100%"
  },

}));

// Individual PlantCard
function PlantCard({ instance }: any) {
  const classes = useStyles();
  return (
    <Link to={`/plants/` + instance.id} style={{ textDecoration: 'none' }}>

    <CardActionArea className={classes.cardActionArea} >
      <Card className={classes.card}>

        <CardMedia className={classes.cardMedia} image={instance.image_url} title={instance.name} />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">{instance.common_name}</Typography>
          <Typography display="inline">Genus: </Typography>
          <Typography display="inline" style={{ fontStyle: "italic" }}>{instance.genus}</Typography>
          <Typography></Typography>
          <Typography display="inline">Scientific Name: </Typography>
          <Typography display="inline" style={{ fontStyle: "italic" }}>{instance.scientific_name}</Typography>
          <Typography></Typography>
          <Typography display="inline">Family: </Typography>
          <Typography display="inline" style={{ fontStyle: "italic" }}>{instance.family}</Typography>
        </CardContent>

      </Card>
    </CardActionArea>
    <Box m={2} >
    </Box>



    </Link>

  );
}


export default PlantCard;
