import React, {Component, useEffect, useState} from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import { Route, useParams, } from "react-router-dom";
import Instances from "../../components/Instances"
import Box from '@material-ui/core/Box';

import { CardActionArea } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";

import axios from 'axios';

import City from "../cities/City";
import Recipe from "../recipes/Recipe";



const plantStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));

// Display content for an individual species page
function Plant() {
  const classes = plantStyles();
  let { plantId }: any = useParams();

  const [plant, setPlant] = useState({"common_name": "sammer squash", "genus": "", "id": "", "family": "", "scientific_name": "", "image_url": "", "wiki_url": "", "vegetable": ""});

  useEffect(() => {
    axios.get('http://0.0.0.0/api/Plants/' + plantId)
    .then(response => {
      console.log("USEEFECT: " + JSON.stringify(response.data));
      setPlant(response.data);
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    })
  }, [])

  if(plant.common_name === "sammer squash"){
    return  (<h3 className="text-center">Plant not found.</h3>)
  }

  else{
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">
            {plant.common_name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {plant.scientific_name}
          </Typography>
        </Container>

        <Container  maxWidth="md">
          <Grid container spacing={4}>
              <Grid item xs >
                <Card style={{ textAlign: "center" }}>
                <CardActionArea href={plant.image_url}>
                  <CardMedia className={classes.media} image={plant.image_url} title={plant.common_name} />
                  <CardContent>{plant.common_name}</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item xs >
                <Card style={{  textAlign: "center"  }}>
                  <CardActionArea href={plant.wiki_url}>
                    <CardMedia className={classes.media}  image="https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/1200px-Wikipedia-logo-v2.svg.png" title="Wikipedia" />
                    <CardContent>Wikipedia</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>

          </Grid>
          <Grid container spacing={4}>
              <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <PlantTable key={plant.common_name} plant={plant} />
              </Card>
              </Grid>
          </Grid>

          <Box m={2} >
            <Typography  variant="h4" align="center" color="textPrimary" >
            Related
            </Typography>
          </Box>

          <RelatedCards id={plant.id}/>
        </Container>
      </div>
    )
  }
}

function PlantTable({ plant }: any) {

  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Family</TableCell>
            <TableCell align="center">{plant.family}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Genus</TableCell>
            <TableCell align="center">{plant.genus}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Scientific Name</TableCell>
            <TableCell align="center">{plant.scientific_name}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Vegetable</TableCell>
            <TableCell align="center">{plant.vegetable.toString()}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Wikipedia URL</TableCell>
            <TableCell align="center"><a href={plant.wiki_url}>{plant.wiki_url}</a></TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}


interface RelatedCardsProps { // Added this interface for props
  id: string,
}

export class RelatedCards extends Component <RelatedCardsProps>{

  state = {
    relatedCities: [],
    relatedRecipes: [],
    loading: false
  };

  componentDidMount() {
    const {id}= this.props;
    const getInstances = async () => {
      this.setState({ loading: true });

      const results_1 = await axios.get("http://0.0.0.0:80/api/Plants/Cities/" + id);
      // console.log("https://jsonplaceholder.typicode.com/posts?id="+id);
      this.setState({ relatedCities: results_1.data.cities });

      const results_2 = await axios.get("http://0.0.0.0:80/api/Plants/Recipes/"+id);
      this.setState({ relatedRecipes: results_2.data.recipes });

      this.setState({ loading: false });
    };

    getInstances();
  }

  render() {
    const { relatedCities, relatedRecipes, loading } = this.state;




    return (

        <div>
            <Grid container spacing={4}>
                <Grid item xs >
                    <Instances instances={relatedCities} card={"CityCard"} loading={loading} />
                </Grid>
                <Grid item xs >
                    <Instances instances={relatedRecipes} card={"RecipeCard"} loading={loading} />
                </Grid>
            </Grid>
            <Route path={`/cities/:cityId`} children={<City />} />
            <Route path={`/recipes/:recipeId`} children={<Recipe />} />
        </div>


    )
  }
}

export default Plant;
