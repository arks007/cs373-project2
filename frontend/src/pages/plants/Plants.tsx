import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import { Route, Link, useRouteMatch, useParams, } from "react-router-dom";
import { CardActionArea } from "@material-ui/core";


const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "56.25%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  cardActionArea: {
    height: "100%"
  },
}));

const cards = [
  {
    name: "Cabbage",
    img: "https://bs.floristic.org/image/o/8078949488c936fb807a242f530d344489a3127e",
    genus: "Brassica",
    species: "oleracea",
    family: "Mustard",
    edible_parts: "stem, leaves, flowers",
    duration: "biennial",
    dz: "California, Texas, New York, and more",
    pathname: "cabbage"
  },
  {
    name: "Kidney Bean",
    img: "https://bs.floristic.org/image/o/46b23b23e98319531962424fb9d88a4656610a0b",
    genus: "Phaseolus",
    species: "vulgaris",
    family: "Pea",
    edible_parts: "fruits, seeds",
    duration: "annual",
    dz: "New York, Illinois",
    pathname: "kidney-bean"
  },
  {
    name: "Garden Tomato",
    img: "https://bs.floristic.org/image/o/400851a79391dbe6f667c66e4bf70299e9921853",
    genus: "Solanum",
    species: "lycopersicum",
    family: "Potato",
    edible_parts: "entire plant",
    duration: "annual",
    dz: "California, Texas, New York, and more",
    pathname: "garden-tomato"
  }
];

// Individual PlantCard
function PlantCard({ plant }: any) {
  const classes = useStyles();
  let match = useRouteMatch();
  return (
    <Link to={`${match.url}/${plant.pathname}`} style={{ textDecoration: 'none' }}>
      <CardActionArea className={classes.cardActionArea}>
        <Card className={classes.card}>

          <CardMedia className={classes.cardMedia} image={plant.img} title={plant.name} />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">{plant.name}</Typography>
            <Typography display="inline">Genus: </Typography>
            <Typography display="inline" style={{ fontStyle: "italic" }}>{plant.genus}</Typography>
            <Typography></Typography>
            <Typography display="inline">Species: </Typography>
            <Typography display="inline" style={{ fontStyle: "italic" }}>{plant.species}</Typography>
            <Typography>Common family name: {plant.family}</Typography>
            <Typography>Edible parts: {plant.edible_parts}</Typography>
            <Typography>Duration: {plant.duration}</Typography>
            <Typography>Suitable for: {plant.dz}</Typography>
          </CardContent>

        </Card>
      </CardActionArea>
    </Link>

  );
}

export default function Plants() {
  const classes = useStyles();
  let match = useRouteMatch();
  return (
    <React.Fragment>
      <Route exact path="/plants">

        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">Plants</Typography>
        </Container>

        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            {cards.map((plant) => (
              <Grid item xs={12} sm={6} md={4}>
                <PlantCard key={plant.name} plant={plant} />
              </Grid>
            ))}
          </Grid>
        </Container>
      </Route>
      <Route path={`${match.path}/:plantId`} children={<Plant />} />
    </React.Fragment>
  );
}

const plantStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '75%', // 16:9
  },
}));

// Display content for an individual species page
function Plant() {
  const classes = plantStyles();
  let { plantId }: any = useParams();
  let plant = cards.find((card) => card.pathname === plantId);
  if (plant) {
    return (
      <div className={classes.root}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary">
            {plant.name}
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" style={{ fontStyle: "oblique" }} paragraph>
            {plant.genus} {plant.species}
          </Typography>
        </Container>

        <Container maxWidth="md">
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6}>
              <Card style={{ textAlign: "center" }}>
                <CardMedia className={classes.media} image={plant.img} title={plant.name} />
              </Card>
            </Grid>
            <Grid item xs={12} sm={6}>
              <PlantTable key={plant.name} plant={plant} />
            </Grid>
          </Grid>
        </Container>
      </div>
    );
  }
  return (
    <h3 className="text-center">Plant not found.</h3>
  );
}

const tableStyles = makeStyles({
  table: {
    maxWidth: 445,
  },
});

function PlantTable({ plant }: any) {
  const classes = tableStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableBody>
          <TableRow>
            <TableCell align="center">Genus</TableCell>
            <TableCell align="center">{plant.genus}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Species</TableCell>
            <TableCell align="center">{plant.species}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Duration</TableCell>
            <TableCell align="center">{plant.duration}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell align="center">Edible parts</TableCell>
            <TableCell align="center">{plant.edible_parts}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}
