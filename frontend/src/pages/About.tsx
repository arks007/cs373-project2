import React, { useEffect, useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import ShareIcon from '@material-ui/icons/Share';
import ListAltIcon from '@material-ui/icons/ListAlt';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import Paper from '@material-ui/core/Paper';
import { CardActionArea } from "@material-ui/core";

const retrieveRepositoryStats = async () => {
  let numCommits = 0, numIssues = 0, numTests = 0;

  // Handle Commits
  let response = await fetch("https://gitlab.com/api/v4/projects/21176938/repository/contributors");
  let contributors = await response.json();
  contributors.forEach(function (contributor: any) {
    const { name, commits } = contributor
    cards.forEach(function (card) {
      if (card.heading === name) {
        card.num_commits += commits
      }
    })
    numCommits += commits;
  });

  // Handle Issues
  response = await fetch("https://gitlab.com/api/v4/projects/21176938/issues")
  let issues = await response.json();
  issues.forEach(function (issue: any) {
    const { assignees } = issue
    if (assignees.length !== 0) {
      assignees.forEach(function (assignee: any) {
        let { name } = assignee
        if (name === "Jonathan H Randall") {
          name = "Jonathan Randall"
        }
        cards.forEach(function (card) {
          if (card.heading === name) {
            card.num_issues += 1
          }
        })
      })
    }
    numIssues += 1;
  });

  // TODO: Handle Tests

  return {
    numCommits: numCommits,
    numIssues: numIssues,
    numTests: numTests,
    cards: cards
  }
}

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  cardMedia: {
    paddingTop: "100%" // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },


}));

const cards = [
  {
    heading: "Jonathan Randall",
    img: "https://i.imgur.com/EnIv0gi.png",
    bio: "I'm a third year CS and Plan II major at UT Austin. I'm from Dallas, Texas, and I love producing music, cooking and looking at pictures of buildings online.",
    resp: "Front-end",
    num_commits: 0,
    num_issues: 0,
    unit_tests: 0
  },
  {
    heading: "Grant He",
    img: "https://i.imgur.com/FgthWRz.jpg",
    bio: "I'm a third year CS major at UT Austin. I was born and raised in the Austin area and haven't stopped repping the Longhorns since. In my free time I enjoy cooking, watching screwball comedies, and backpacking the great outdoors.",
    resp: "Front-end",
    num_commits: 0,
    num_issues: 0,
    unit_tests: 0
  },
  {
    heading: "Sujoy Purkayastha",
    img: "https://miro.medium.com/max/462/1*kRfTgLaIzegCtbQdk1SDLw.png",
    bio: "I'm a third year CS major at UT Austin. I'm from Plano, Texas and I enjoy fishkeeping and exploring downtown Austin.",
    resp: "Back-end",
    num_commits: 0,
    num_issues: 0,
    unit_tests: 0
  },
  {
    heading: "Pranav Akinepalli",
    img: "https://i.imgur.com/Nh8jf6E.jpg",
    bio: "I'm a third year CS major and Business minor at UT Austin. I'm from Irving, Texas and have lived there for most of my life. My hobbies include playing video games, spending time outside in nature, and traveling (so I can try lots of different foods)!",
    resp: "Back-end",
    num_commits: 0,
    num_issues: 0,
    unit_tests: 0
  },
  {
    heading: "Sameer Haniyur",
    img: "https://i.imgur.com/xsNpZQc.jpg",
    bio: "I'm a third year CS major living in Flower Mound, Texas. I'm originally from Florida just outside Miami, but I moved to Texas when I was one. My favorite hobby outside of some light gaming is playing golf on the hundreds of courses in the Dallas area.",
    resp: "Back-end",
    num_commits: 0,
    num_issues: 0,
    unit_tests: 0
  }
];

export default function Album() {

  const [numCommits, setNumCommits]: [number, any] = useState(0);
  const [numIssues, setNumIssues]: [number, any] = useState(0);
  const [numTests, setNumTests]: [number, any] = useState(0);
  const [cards, setCards]: [any, any] = useState([]);

  useEffect(() => {
    const retrieveData = async () => {
      const repositoryStats = await retrieveRepositoryStats();
      // Set up stats for first call only
      if (cards.length === 0) {
        setNumCommits(repositoryStats.numCommits);
        setNumIssues(repositoryStats.numIssues);
        setNumTests(repositoryStats.numTests);
        setCards(repositoryStats.cards);
      }
    }
    retrieveData();
  });

  const classes = useStyles();

  return (
    <React.Fragment>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>About</Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              How do we feed our urbanizing world?
              How do we make our food systems resilient?
              Our goal is to promote urban agriculture and enhance urban food security.
            </Typography>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Typography variant="h3" align="center" color="textPrimary" gutterBottom>Our Team</Typography>
          <Grid container spacing={4}>
            {cards.map((card:any) => (
              <Grid item key={card} xs={12} sm={6} md={4} >
                <Card className={classes.card}>
                  <CardMedia className={classes.cardMedia} image={card.img} title={card.heading} />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {card.heading}
                    </Typography>

                    <Typography gutterBottom variant="h6" component="h2">
                      {card.resp}
                    </Typography>

                    <Typography>{card.bio}</Typography>

                  </CardContent>
                  <CardActions disableSpacing>

                    <div className={classes.root}>
                      <Grid container spacing={0}>

                        <Grid item xs={4}>
                          <Paper className={classes.paper} elevation={0}>
                            <Typography gutterBottom component="h2">Commits</Typography>
                            <Grid container>
                              <Grid item xs={12} sm={6}>
                                <ShareIcon />
                              </Grid>
                              <Grid item xs={12} sm={6}>
                                <Typography>{card.num_commits}</Typography>
                              </Grid>
                            </Grid>
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper className={classes.paper} elevation={0}>
                            <Typography gutterBottom component="h2">Issues</Typography>
                            <Grid container>
                              <Grid item xs={12} sm={6}>
                                <ListAltIcon />
                              </Grid>
                              <Grid item xs={12} sm={6}>
                                <Typography>{card.num_issues}</Typography>
                              </Grid>
                            </Grid>
                          </Paper>
                        </Grid>
                        <Grid item xs={4}>
                          <Paper className={classes.paper} elevation={0} >
                            <Typography gutterBottom component="h2">Tests</Typography>
                            <Grid container>
                              <Grid item xs={12} sm={6}>
                                <CheckCircleOutlineIcon />
                              </Grid>
                              <Grid item xs={12} sm={6}>
                                <Typography>{card.unit_tests}</Typography>
                              </Grid>
                            </Grid>
                          </Paper>
                        </Grid>
                      </Grid>
                    </div>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>

        <Grid container direction="column" alignItems="center">
          <Grid item>
            <Typography variant="h3" align="center" color="textPrimary"  style={{ marginTop: 10, marginBottom: 5 }}>Repository Statistics</Typography>
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={2}>
              <Grid item>
                <Card style={{ height: "125px", width: "250px", textAlign: "center" }}>
                  <CardContent>
                    <Typography gutterBottom variant="h4" component="h2">Commits</Typography>
                    <Typography variant="h6">{numCommits}</Typography>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "125px", width: "250px", textAlign: "center" }}>
                  <CardContent>
                    <Typography gutterBottom variant="h4" component="h2">Issues</Typography>
                    <Typography variant="h6">{numIssues}</Typography>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "125px", width: "250px", textAlign: "center" }}>
                  <CardContent>
                    <Typography gutterBottom variant="h4" component="h2">Tests</Typography>
                    <Typography variant="h6">{numTests}</Typography>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container direction="column" alignItems="center">
          <Grid item>
            <Typography variant="h3" align="center" color="textPrimary"  style={{ marginTop: 10, marginBottom: 5 }}>APIs Utilized</Typography>
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={2}>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://trefle.io/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/j9cvWTK.png" title="Trefle Icon" />
                    <CardContent>Trefle</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://www.edamam.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/RKea6xH.png" title="Edamam Icon" />
                    <CardContent>Edamam</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://openweathermap.org/api">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/T9FutE2.png" title="Open Weather Icon" />
                    <CardContent>OpenWeather</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid container direction="column" alignItems="center">
          <Grid item>
            <Typography variant="h3" align="center" color="textPrimary"  style={{ marginTop: 10, marginBottom: 5 }}>Development Tools</Typography>
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={2}>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://reactjs.org/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/mRZA6lX.png" title="React Icon" />
                    <CardContent>React</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://material-ui.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/0AyGOAM.png" title="Material UI Icon" />
                    <CardContent>Material UI</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://aws.amazon.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/qkEhDRo.png" title="AWS Icon" />
                    <CardContent>AWS</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={2} style={{marginTop: 10}}>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://www.docker.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/oj614EO.png" title="Docker Icon" />
                    <CardContent>Docker</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://gitlab.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/dLQSqdI.png" title="GitLab Icon" />
                    <CardContent>GitLab</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://postman.com/">
                    <CardMedia style={{ height: "200px", width: "250px" }} image="https://i.imgur.com/67OZVHH.png" title="Postman Icon" />
                    <CardContent>Postman</CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid container direction="column" alignItems="center">
          <Grid item>
            <Typography variant="h3" align="center" color="textPrimary" style={{ marginTop: 10 }}>Links</Typography>
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={2}>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://gitlab.com/jonathanhrandall/cs373-project2/">
                    <CardMedia style={{ height: "250px", width: "250px" }} image="https://i.imgur.com/dLQSqdI.png" title="GitLab Icon" />
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item>
                <Card style={{ height: "250px", width: "250px", textAlign: "center" }}>
                  <CardActionArea href="https://documenter.getpostman.com/view/12307802/TVRdAX6w">
                    <CardMedia style={{ height: "250px", width: "250px" }} image="https://i.imgur.com/67OZVHH.png" title="Postman Icon" />
                  </CardActionArea>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </main>
    </React.Fragment>
  );
}
