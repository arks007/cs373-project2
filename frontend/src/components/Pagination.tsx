import React, { Component } from 'react'


interface PaginationProps { // Added this interface for props
  instancesPerPage : any,
  totalInstances : any,
  paginate : any,
  nextPage : any,
  prevPage : any;
}

export class Pagination extends Component<PaginationProps> {
    render() {
        const { instancesPerPage, totalInstances, paginate, nextPage, prevPage } = this.props;

        const pageNumbers: number[] = [];

        for (let i = 1; i <= Math.ceil(totalInstances / instancesPerPage); i++) {
            pageNumbers.push(i);
        }

        return (
            <nav>
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => prevPage()}>Previous</a>
                    </li>
                    {pageNumbers.map(num => (
                        <li className="page-item" key={num}>
                            <a onClick={() => paginate(num)} href="#" className="page-link">{num}</a>
                        </li>
                    ))}
                    <li className="page-item">
                        <a className="page-link" href="#" onClick={() => nextPage()}>Next</a>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default Pagination
