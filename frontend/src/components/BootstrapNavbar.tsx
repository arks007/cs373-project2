import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap'
import Home from '../pages/Home';
import Plants from '../pages/plants/PlantGrid';
import Cities from '../pages/cities/CityGrid';
import Recipes from '../pages/recipes/RecipeGrid';
import About from '../pages/About';
import HomeFarmerLogo from '../hflogo.svg';

class BootstrapNavbar extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Navbar variant='dark' expand='lg' sticky='top' style={{ background: '#00796b' }}>
            <Navbar.Brand href='/'>
              <img src={HomeFarmerLogo} alt='HomeFarmerLogo' style={{ marginRight: 5 }} />HomeFarmer
            </Navbar.Brand>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse id='basic-navbar-nav'>
              <Nav style={{ background: '#00796b' }}>
                <Nav.Link href='/plants'>Plants</Nav.Link>
                <Nav.Link href='/cities'>Cities</Nav.Link>
                <Nav.Link href='/recipes'>Recipes</Nav.Link>
                <Nav.Link href='/about'>About</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <br />
          <Switch>
            <Route exact path='/'>
              <Home />
            </Route>
            <Route path='/about'>
              <About />
            </Route>
            <Route path='/plants'>
              <Plants />
            </Route>
            <Route path='/cities'>
              <Cities />
            </Route>
            <Route path='/recipes'>
              <Recipes />
            </Route>
          </Switch>
        </Router>
      </div>
    )
  }
}

export default BootstrapNavbar;
