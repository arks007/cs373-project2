import React, { Component } from 'react';

import PlantCard from "../pages/plants/PlantCard"
import CityCard from "../pages/cities/CityCard"
import RecipeCard from "../pages/recipes/RecipeCard"

//import Recipe from "../recipes/Recipe"



interface InstancesProps { // Added this interface for props
  instances: any,
  card: string,
  loading: boolean,
}

export class Instances extends Component <InstancesProps>{
    render() {
        const { instances, card, loading } = this.props;

        if (loading) {
            return <h2>Loading...</h2>
        }

        if(card === "PlantCard"){
          return (

              <div>


                      {instances.map( (instance:any) => (
                          <PlantCard key={instance.id} instance={instance} />
                      ))}


              </div>
          )

        }

        else if(card === "RecipeCard"){
          return (

              <div>


                      {instances.map( (instance:any) => (
                          <RecipeCard key={instance.id} instance={instance} />
                      ))}


              </div>
          )

        }

        else if(card === "CityCard"){
          return (

              <div>


                      {instances.map( (instance:any) => (
                          <CityCard key={instance.id} instance={instance} />
                      ))}


              </div>
          )

        }

    }
}


export default Instances;
