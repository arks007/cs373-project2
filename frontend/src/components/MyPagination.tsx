import React, { Component } from 'react'
import { MemoryRouter, Route } from 'react-router';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';

interface PaginationProps { // Added this interface for props
  instancesPerPage : any,
  totalInstances : any,
  paginate : any,
}


export class MyPagination extends Component<PaginationProps> {
  render() {

    const { instancesPerPage, totalInstances, paginate, } = this.props;

    let numPages = Math.ceil(totalInstances / instancesPerPage);


    return(
    <MemoryRouter initialEntries={['/cities']} initialIndex={0}>
      <Route>
        {({ location }) => {
          const query = new URLSearchParams(location.search);

          const page = parseInt(query.get('page') || '1', 10);
          console.log(page)
          return (
            <Pagination
              page={page}
              count={numPages}
              renderItem={(item) => (



                 <PaginationItem component={Link}
                  to={`/cities${item.page === 1 ? '' : `?page=${item.page}`}`}
                  {...item}

                    onClick={() => paginate(item.page)}


                 >
                 </PaginationItem>

              )}
            />
          );
        }}
      </Route>
    </MemoryRouter>
    )
  }
}


export default MyPagination
