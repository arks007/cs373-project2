import React from 'react';
import GoogleMapReact from 'google-map-react';

import './Map.css';

type MapSectionProps = {
  location: any,
  zoomLevel: number,
  mapTypeId: string
}

function MapSection({ location, zoomLevel, mapTypeId }: MapSectionProps) {
  return (
    <div className="map">
      <div className="google-map">
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_MAPS_API_KEY! }}
          defaultCenter={location}
          defaultZoom={zoomLevel}
          options={function () { return { mapTypeId: mapTypeId } }}
        >
        </GoogleMapReact>
      </div>
    </div>);
}

export default MapSection;