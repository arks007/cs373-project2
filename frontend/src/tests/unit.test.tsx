import { mount } from 'enzyme';
import React from 'react';
import GoogleMapReact from 'google-map-react';
// import BootstrapNavbar from '../components/BootstrapNavbar';
import MapSection from '../components/Map';

// Author: Grant
test('MapSection contains GoogleMapReact', () => {
    const wrapper = mount(<MapSection location={{ address: 'Austin', lat: 30.27, lng: -97.74 }} zoomLevel={10} mapTypeId="hybrid" />);
    expect(wrapper.contains(<GoogleMapReact />));
});

// Author: Grant
// test('BootstrapNavbar contains proper links', () => {
//     const wrapper = shallow(<BootstrapNavbar />);
//     expect(wrapper.find('Switch')).toHaveLength(5);
// });