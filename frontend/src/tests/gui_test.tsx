import { Builder, Capabilities, By } from 'selenium-webdriver';
import chrome from 'selenium-webdriver/chrome';
import chromedriver from 'chromedriver';

chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());

const driver = new Builder().withCapabilities(Capabilities.chrome()).build();
const base_url = "https://www.homefarmer.me/";

driver.manage().window().maximize();
jest.setTimeout(30000);

// Author: Grant He
test('has correct title', async () => {
    await driver.get(base_url);
    await driver.sleep(1500);
    const title = await driver.getTitle();
    expect(title).toEqual('HomeFarmer');
});

// Author: Grant He
test('can navigate to plants', async () => {
    await driver.get(base_url);
    await driver.sleep(1500);
    const plants_link = (await driver).findElement(By.linkText('Plants'));
    plants_link.click();
    await driver.sleep(1500);
    await driver.getCurrentUrl().then((curr_url: string) => expect(curr_url).toEqual(expect.stringContaining(base_url + 'plants')));
});

// Author: Grant He
test('can navigate to cities', async () => {
    await driver.get(base_url);
    await driver.sleep(1500);
    const cities_link = (await driver).findElement(By.linkText('Cities'));
    cities_link.click();
    await driver.sleep(1500);
    await driver.getCurrentUrl().then((curr_url: string) => expect(curr_url).toEqual(expect.stringContaining(base_url + 'cities')));
});

// Author: Grant He
test('can navigate to recipes', async () => {
    await driver.get(base_url);
    await driver.sleep(1500);
    const recipes_link = (await driver).findElement(By.linkText('Recipes'));
    recipes_link.click();
    await driver.sleep(1500);
    await driver.getCurrentUrl().then((curr_url: string) => expect(curr_url).toEqual(expect.stringContaining(base_url + 'recipes')));
});